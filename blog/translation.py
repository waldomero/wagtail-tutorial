from wagtailtrans.models import TranslatablePage

from .models import TransHomePage, BlogPage, PostPage, LandingPage, FormPage, FreelancerPage, TransLandingPage

from modeltranslation.translator import TranslationOptions
from modeltranslation.decorators import register


@register(TransHomePage)
class TransHomePageTR(TranslationOptions):
    fields = (
        'body',
    )


@register(BlogPage)
class BlogPageTR(TranslationOptions):
    fields = ()


@register(PostPage)
class PostPageTR(TranslationOptions):
    fields = ()


@register(LandingPage)
class LandingPageTR(TranslationOptions):
    fields = ()


@register(FormPage)
class FormPageTR(TranslationOptions):
    fields = ()


@register(FreelancerPage)
class FreelancerPageTR(TranslationOptions):
    fields = ()


@register(TranslatablePage)
class TranslatablePageTR(TranslationOptions):
    fields = ()


@register(TransLandingPage)
class TransLandingPageTR(TranslationOptions):
    fields = ()
